from django.urls import path, include
from blog.views import blog_posts_display

urlpatterns = [
    path('', blog_posts_display, name='blog_posts_display'),
]
