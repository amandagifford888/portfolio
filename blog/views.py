from django.shortcuts import render
from blog.models import BlogPost

def blog_posts_display(request):
    posts = BlogPost.objects.all()
    context = {
        "posts": posts,
    }
    return render(request, "blog/posts.html", context)
