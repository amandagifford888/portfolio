from django.db import models
from django.conf import settings


class BlogPost(models.Model):
    title = models.CharField(max_length=200)
    post = models.TextField()
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="blogs",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
