from django.urls import path
from amanda.views import show_page

urlpatterns = [
    path('', show_page, name="show_page"),
]
